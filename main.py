# Computes statistics for Can't Stop game.
#
# In Can't Stop, you roll four dice and associate them in pairs.
# You have three totems to advance and take columns.


print("Statistics for Can't Stop board game!")
print()

## 1- Compute sttatistics to get a column in one throw

# initialize each possible sum of two dice
columns = {}
for prob in range(2,13):
    columns[prob] = 0
    
def createSums(die1,die2,die3,die4):
    return set([ die1 + die2, die1 + die3, die1 + die4, die2 + die3, die2 + die4, die3 + die4])

for die1 in range(1,7):
    for die2 in range(1,7):
        for die3 in range(1,7):
            for die4 in range(1,7):
                sums = createSums(die1,die2,die3,die4)
                for s in sums:
                    columns[s] += 1

throws_count = 6*6*6*6

print("Probability to get a given column with one dice throw")
dash = '-' * 20
print(dash)
print("{0:<10s}{1:>10s}".format("COLUMN", "(%)"))
print(dash)
for k,v in columns.items():
    print("{0:<10d}{1:>9.1f}%".format(k, v*100.0 / throws_count))
print()

## 2- Compute statistics to get one of the three columns you have put a totem into
# Get all totems combinations
totems = {}
for totem1 in range(2,13):
    for totem2 in range(2,13):
        for totem3 in range(2,13):
            if (totem1 != totem2 and totem1 != totem3 and totem2 != totem3):
                key = [totem1, totem2, totem3]
                key.sort()
                totems[tuple(key)] = 0

for die1 in range(1,7):
    for die2 in range(1,7):
        for die3 in range(1,7):
            for die4 in range(1,7):
                sums = createSums(die1,die2,die3,die4)
                for totemtriplet in totems:
                    for totem in totemtriplet:
                        if (totem in sums):
                            totems[totemtriplet] += 1
                            break

print("One the three totems are placed, probability to advance a totem with one dice throw")
dash = '-' * 22
print(dash)
print("{0:<12s}{1:>10s}".format("TOTEMS", "(%)"))
print(dash)
for k,v in totems.items():
    print("{0:<12s}{1:>9.1f}%".format(str(k), v*100.0 / throws_count))
print()